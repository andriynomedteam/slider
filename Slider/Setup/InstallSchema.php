<?php

namespace Andriynomed\Slider\Setup;



use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;



class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /*
         * Drop tables if exists
         */
        $installer->getConnection()->dropTable($installer->getTable('andriynomed_slider_slide'));

        /*
         * Create table andriynomed_slider_slide
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('andriynomed_slider_slide')
        )->addColumn(
            'slide_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Slide ID'
		)->addColumn(
            'is_enabled',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Slide enabled?'	
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Slide title'
		)->addColumn(
            'is_show_title',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true, 'default' => '0'],
            'Show title?'
		 )->addColumn(
            'description',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true, 'default' => ''],
            'Slide description/content'
        )->addColumn(
            'is_show_description',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true, 'default' => '0'],
            'Show description?'	
		)->addColumn(
            'image_src',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Slide image src'
        )->addColumn(
            'image_alt',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Slide image alt'
		)->addColumn(
            'slide_type',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => '0'],
            'Slide type'
		)->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'unsigned' => true],
            'Product ID'	
		)->addColumn(
            'image_href',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => ''],
            'Slide href'	
        )->addColumn(
            'slide_order',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'default' => 0],
            'Slide order'
        )->addColumn(
            'slider_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Slider Id'
        )->addColumn(
            'target',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true, 'default' => '0'],
            'Slide target'
        )->addColumn(
            'from_time',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Slide starting time'
        )->addColumn(
            'to_time',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Slide ending time'
        )->addIndex(
            $installer->getIdxName('andriynomed_slider_slide', ['slide_id']),
            ['slide_id']
        )->addIndex(
            $installer->getIdxName('andriynomed_slider_slide', ['slider_id']),
            ['slider_id']
        )->addIndex(
            $installer->getIdxName('andriynomed_slider_slide', ['is_enabled']),
            ['is_enabled']
        )->addIndex(
            $installer->getIdxName('andriynomed_slider_slide', ['from_time']),
            ['from_time']
        )->addIndex(
            $installer->getIdxName('andriynomed_slider_slide', ['to_time']),
            ['to_time']
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table andriynomed_slider_slide
         */

        $installer->endSetup();
    }
}
