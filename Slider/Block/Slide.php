<?php

namespace Andriynomed\Slider\Block;


use Andriynomed\Slider\Model\Status;



class Slide extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Andriynomed\Slider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;
    
     /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Andriynomed\Slider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Andriynomed\Slider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_slideCollectionFactory = $slideCollectionFactory;
    }

    /**
     * @return \Andriynomed\Slider\Model\ResourceModel\Slide\Collection
     */
    public function getSlides()
    {
        if (!$this->hasData('slides')) {
            $slides = $this->_slideCollectionFactory->create()
                ->addFieldToFilter('is_enabled', Status::STATUS_ENABLED)
                ->setOrder('slide_order', 'DESC');
            $this->setData('slides', $slides);
        }
        return $this->getData('slides');
    }
}