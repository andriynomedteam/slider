<?php

namespace Andriynomed\Slider\Controller;



use \Magento\Framework\App\Action\Action;



class Index extends Action
{

    /**
     * A result that contains raw response - may be good for passing through files
     * returning result of downloads or some other binary contents.
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $_resultRawFactory;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context                                $context
     * @param \Magento\Framework\Controller\Result\RawFactory                      $resultRawFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone
    ) {
        parent::__construct($context);

        $this->_resultRawFactory = $resultRawFactory;
    }

    public function execute()
    {
        return $this->_resultRawFactory->create();
    }
}
