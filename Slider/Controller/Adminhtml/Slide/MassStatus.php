<?php

namespace Andriynomed\Slider\Controller\Adminhtml\Slide;



use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Andriynomed\Slider\Model\ResourceModel\Slide\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Andriynomed\Slider\Model\Status;


class MassStatus extends \Magento\Backend\App\Action
{
        /**
     * MassActions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }


    /**
     * Update product(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        //значення status в xml задаються вручну імають відповідати Status::STATUS_DISABLED, Status::STATUS_ENABLED
        $status = (int) $this->getRequest()->getParam('status');
        $status = ($status == Status::STATUS_DISABLED) ? Status::STATUS_DISABLED: Status::STATUS_ENABLED;
        $statusChanged = 0;
        foreach ($collection as $item)
        {
            if( (int)$item->isEnabled() != $status)
            {
                $item->setEnabled($status);
                $item->save();
                $statusChanged++;
            }
        }
        if($status == Status::STATUS_ENABLED)
        {
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been enabled.', $statusChanged));
        } else {
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been disabled.', $statusChanged));
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Andriynomed_Slider::slide');
    }
}
