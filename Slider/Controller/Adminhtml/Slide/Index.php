<?php

namespace Andriynomed\Slider\Controller\Adminhtml\Slide;



use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;



class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Andriynomed_Slider::slider_slides');
        $resultPage->addBreadcrumb(__('Slides for Sliders'), __('Slides for Sliders'));
        $resultPage->addBreadcrumb(__('Manage Slides'), __('Manage Slides'));
        $resultPage->getConfig()->getTitle()->prepend(__('Slides'));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the slide grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Andriynomed_Slider::slider_slides');
    }
    
 


}
