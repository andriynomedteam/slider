<?php

namespace Andriynomed\Slider\Model;

class Status
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

	const SHOW_YES = 1;
	const SHOW_NO = 2;
    /**
     * get available statuses.
     *
     * @return []
     */
    public static function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
	
	public static function getAvailableShowStatuses()
    {
        return [self::SHOW_YES => __('Yes'), self::SHOW_NO => __('No')];
    }
}
