<?php
namespace Andriynomed\Slider\Model\Slide\Source;

use Andriynomed\Slider\Model\Status;

class IsEnabled implements \Magento\Framework\Data\OptionSourceInterface
{
//    /**
//     * @var \Andriynomed\Slider\Model\Slide
//     */
//    protected $_slide;
//
//    /**
//     * Constructor
//     *
//     * @param \Andriynomed\Slider\Model\Slide $slide
//     */
//    public function __construct(\Andriynomed\Slider\Model\Slide $slide)
//    {
//        $this->_slide = $slide;
//    }
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = Status::getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}