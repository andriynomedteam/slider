<?php

namespace Andriynomed\Slider\Model;



use Andriynomed\Slider\Api\Data\SlideInterface;
//use Magento\Framework\DataObject\IdentityInterface;



class Slide extends \Magento\Framework\Model\AbstractModel implements SlideInterface
{
    /* IdentityInterface fields begin */
    
    
    
    /* IdentityInterface fields end */
    
    /* Slide fields begin */
    
    //const BASE_MEDIA_PATH = 'andriynomed/slider/images';//??????
    
    /* Slide fields end */
    
    /* Slide additional functions begin */
    
    /**
     * get availabe slide type. used in Back_end Перемістити в helper???
     *
     * @return []
     */
    public function getAvailableSlideType()
    {
        return [self::SLIDE_TYPE_NORMAL => __('Normal'), self::SLIDE_TYPE_PRODUCT => __('Product')];
    }
    
     /**
     * get target value. used in Back_end Перемістити в helper???
     *
     * @return []
     */
    public function getAvalibleTarget()
    {
        return [self::SLIDE_TARGET_SELF => __('New Window with Browser Navigation'),
                self::SLIDE_TARGET_PARENT => __('Parent Window with Browser Navigation'),
                self::SLIDE_TARGET_BLANK => __('New Window without Browser Navigation')];
    }
    
    /* Slide additional functions end */
    
    /* Slide required functions begin */
    
    /**
     * [__construct description].
     *
     * @param \Magento\Framework\Model\Context                                 $context
     * @param \Magento\Framework\Registry                                      $registry
     * @param \Andriynomed\Slider\Model\ResourceModel\Slide                   $resource
     * @param \Andriynomed\Slider\Model\ResourceModel\Slide\Collection        $resourceCollection
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Andriynomed\Slider\Model\ResourceModel\Slide $resource,
        \Andriynomed\Slider\Model\ResourceModel\Slide\Collection $resourceCollection
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection);
    }
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Andriynomed\Slider\Model\ResourceModel\Slide');
    }
    
   
    
    /* Slide required functions end */

    /* SlideInterface field getter/setter begin */
    
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::SLIDE_ID);
    }

    /**
     * Is enabled
     *
     * @return bool|null
     */
    public function isEnabled()
    {
        //return (bool) $this->getData(self::IS_ENABLED);
        return $this->getData(self::IS_ENABLED) == Status::STATUS_ENABLED ? true : false;
    }
    
    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Is show title
     *
     * @return bool|null
     */
    public function isShowTitle()
    {
        //return (bool) $this->getData(self::IS_SHOW_TITLE);
        return $this->getData(self::IS_SHOW_TITLE) == Status::SHOW_YES ? true : false;
    }
    
    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Is show description
     *
     * @return bool|null
     */
    public function isShowDescription()
    {
        //return (bool) $this->getData(self::IS_SHOW_DESCRIPTION);
        return $this->getData(self::IS_SHOW_DESCRIPTION) == Status::SHOW_YES ? true : false;
    }
    
    /**
     * Get image src
     *
     * @return string|null
     */
    public function getImageSrc()
    {
        return $this->getData(self::IMAGE_SRC);
    }

    /**
     * Get image alt
     *
     * @return string|null
     */
    public function getImageAlt()
    {
        return $this->getData(self::IMAGE_ALT);
    }
    
    /**
     * Get slide type
     *
     * @return string|null
     */
    public function getSlideType()
    {
        return $this->getData(self::SLIDE_TYPE);
    }
    
    /**
     * Get product ID
     *
     * @return int|null
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * Get image href
     *
     * @return string|null
     */
    public function getImageHref()
    {
        return $this->getData(self::IMAGE_HREF);
    }
    
    /**
     * Get order
     *
     * @return int|null
     */
    public function getSlideOrder()
    {
        return $this->getData(self::SLIDE_ORDER);
    }
    
    /**
     * Get slider ID
     *
     * @return int|null
     */
    public function getSliderId()
    {
        return $this->getData(self::SLIDER_ID);
    }

    /**
     * Get target
     *
     * @return int|null
     */
    public function getTarget()
    {
        return $this->getData(self::TARGET);
    }
    
    /**
     * Get from time
     *
     * @return string|null
     */
    public function getFromTime()
    {
        return $this->getData(self::FROM_TIME);
    }
    
    /**
     * Get to time
     *
     * @return string|null
     */
    public function getToTime()
    {
        return $this->getData(self::TO_TIME);
    }

    
    
    /**
     * Set ID
     *
     * @param int $id
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setId($id)
    {
        return $this->setData(self::SLIDE_ID, $id);
    }

    /**
     * Set is enabled
     *
     * @param int|bool $enabled
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setEnabled($enabled)
    {
        return $this->setData(self::IS_ENABLED, $enabled);
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set is show title
     *
     * @return int|bool $showTitle
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setShowTitle($showTitle)
    {
        return $this->setData(self::IS_SHOW_TITLE, $showTitle);
    }
    
    /**
     * Set description
     *
     * @param string $description
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Set is show description
     *
     * @param int|bool $isShowDescription
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setShowDescription($showDescription)
    {
        return $this->setData(self::IS_SHOW_DESCRIPTION, $showDescription);
    }
    
    /**
     * Set image src
     *
     * @param string $imageSrc
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setImageSrc($imageSrc)
    {
        return $this->setData(self::IMAGE_SRC, $imageSrc);
    }

    /**
     * Set image alt
     *
     * @param string $imageAlt
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setImageAlt($imageAlt)
    {
        return $this->setData(self::IMAGE_ALT, $imageAlt);
    }
    
    /**
     * Set slide type
     *
     * @param string $slideType
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setSlideType($slideType)
    {
        return $this->setData(self::SLIDE_TYPE, $slideType);
    }
    
    /**
     * Set product ID
     *
     * @param int|bool $productId
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Set image href
     *
     * @param string $imageHref
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setImageHref($imageHref)
    {
        return $this->setData(self::IMAGE_HREF, $imageHref);
    }
    
    /**
     * Set order
     *
     * @param int|bool $slideOrder
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setSlideOrder($slideOrder)
    {
        return $this->setData(self::SLIDE_ORDER, $slideOrder);
    }
    
    /**
     * Set slider ID
     *
     * @param int|bool $sliderId
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setSliderId($sliderId)
    {
        return $this->setData(self::SLIDER_ID, $sliderId);
    }

    /**
     * Set target
     *
     * @param int|bool $target
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setTarget($target)
    {
        return $this->setData(self::TARGET, $target);
    }
    
    /**
     * Set from_time
     *
     * @param string $fromTime
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setFromTime($fromTime)
    {
        return $this->setData(self::FROM_TIME, $fromTime);
    }
    
    /**
     * Set to_time
     *
     * @param string $toTime
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setToTime($toTime)
    {
        return $this->setData(self::TO_TIME, $toTime);
    }
    
    /* SlideInterface field getter/setter end */
}
