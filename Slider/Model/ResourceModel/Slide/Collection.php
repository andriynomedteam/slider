<?php

namespace Andriynomed\Slider\Model\ResourceModel\Slide;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /* AbstractCollection fields begin */
    
    /**
     * @var string
     */
    protected $_idFieldName = 'slide_id';
    
    /* AbstractCollection fields end */
    
    /* Collection fields begin */
     
     
     
    /* Collection fields end */
    
    /* Collection additional functions begin */
    
   
    
    /* Collection additional functions end */
    
    
    /* Collection required functions begin */
    
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Andriynomed\Slider\Model\Slide', 'Andriynomed\Slider\Model\ResourceModel\Slide');
    }
    
    

    /* Collection required functions end */
}
