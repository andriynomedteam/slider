<?php

namespace Andriynomed\Slider\Model\ResourceModel;


class Slide extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
		$this->_init('andriynomed_slider_slide', 'slide_id');
    }
}
