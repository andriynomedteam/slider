<?php
namespace Andriynomed\Slider\Api\Data;


interface SlideInterface
{
    /**
     * Constants
     */
    const SLIDE_TARGET_SELF   = 0;
    const SLIDE_TARGET_PARENT = 1;
    const SLIDE_TARGET_BLANK  = 2;
    
    const SLIDE_TYPE_NORMAL  = 0;
    const SLIDE_TYPE_PRODUCT = 1;
    
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const SLIDE_ID            = 'slide_id';
	const IS_ENABLED          = 'is_enabled';
	const TITLE               = 'title';
	const IS_SHOW_TITLE       = 'is_show_title';
	const DESCRIPTION         = 'description';
	const IS_SHOW_DESCRIPTION = 'is_show_description';
	const IMAGE_SRC           = 'image_src';
	const IMAGE_ALT           = 'image_alt';
	const SLIDE_TYPE          = 'slide_type';
    const PRODUCT_ID          = 'product_id';
	const IMAGE_HREF          = 'image_href';
	const SLIDE_ORDER         = 'slide_order';
	const SLIDER_ID           = 'slider_id';
    const TARGET              = 'target';
    const FROM_TIME           = 'from_time';
    const TO_TIME             = 'to_time';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

	/**
     * Is enabled
     *
     * @return bool|null
     */
    public function isEnabled();
	
	/**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Is show title
     *
     * @return bool|null
     */
    public function isShowTitle();
	
	/**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Is show description
     *
     * @return bool|null
     */
    public function isShowDescription();
	
    /**
     * Get image src
     *
     * @return string|null
     */
    public function getImageSrc();

	/**
     * Get image alt
     *
     * @return string|null
     */
    public function getImageAlt();
	
    /**
     * Get slide type
     *
     * @return string|null
     */
    public function getSlideType();
	
	/**
     * Get product ID
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * Get image href
     *
     * @return string|null
     */
    public function getImageHref();
	
	/**
     * Get order
     *
     * @return int|null
     */
    public function getSlideOrder();
	
	/**
     * Get slider ID
     *
     * @return int|null
     */
    public function getSliderId();

	/**
     * Get target
     *
     * @return int|null
     */
    public function getTarget();
	
	/**
     * Get from time
     *
     * @return string|null
     */
    public function getFromTime();
	
	/**
     * Get to time
     *
     * @return string|null
     */
    public function getToTime();

	
	
	/**
     * Set ID
     *
     * @param int $id
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setId($id);

	/**
     * Set is enabled
     *
     * @param int|bool $enabled
     * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setEnabled($enabled);
	
	/**
     * Set title
     *
     * @param string $title
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setTitle($title);

    /**
     * Set is show title
     *
     * @return int|bool $showTitle
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setShowTitle($showTitle);
	
	/**
     * Set description
     *
     * @param string $description
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setDescription($description);

    /**
     * Set is show description
     *
     * @param int|bool $showDescription
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setShowDescription($showDescription);
	
    /**
     * Set image src
     *
     * @param string $imageSrc
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setImageSrc($imageSrc);

	/**
     * Set image alt
     *
     * @param string $imageAlt
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setImageAlt($imageAlt);
	
    /**
     * Set slide type
     *
     * @param string $slideType
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setSlideType($slideType);
	
	/**
     * Set product ID
     *
     * @param int|bool $productId
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setProductId($productId);

    /**
     * Set image href
     *
     * @param string $imageHref
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setImageHref($imageHref);
	
	/**
     * Set order
     *
     * @param int|bool $slideOrder
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setSlideOrder($slideOrder);
	
	/**
     * Set slider ID
     *
     * @param int|bool $sliderId
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setSliderId($sliderId);

	/**
     * Set target
     *
     * @param int|bool $target
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setTarget($target);
	
	/**
     * Set from_time
     *
     * @param string $fromTime
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setFromTime($fromTime);
	
	/**
     * Set to_time
     *
     * @param string $toTime
	 * @return \Andriynomed\Slider\Api\Data\SlideInterface
     */
    public function setToTime($toTime);
	
}